pragma solidity ^0.5.8;

contract Deposit {
    address public owner;
    uint256 public depositCount;
    uint256 public withdrawCount;
    mapping (address => bool) public registered;
    mapping (address => bool) public allowList;

    constructor() public {
        owner = msg.sender;
        allowList[msg.sender] = true;
    }

    event DepositCreated(address wallet, uint256 amount);
    event WithdrawCreated(address wallet, uint256 amount);
    event AddToAllowList(address wallet);
    event RemoveInAllowList(address wallet);

    function deposit () payable public returns(bool) {
        depositCount += 1;
        registered[msg.sender] = true;
        emit DepositCreated(msg.sender, msg.value);
        return true;
    }

    function withdraw (address payable _wallet, uint256 _amount) public returns(bool) {
        require(allowList[msg.sender] == true, 'You not owner!');
        require(address(this).balance >= _amount);
        withdrawCount += 1;
        _wallet.transfer(_amount);
        emit WithdrawCreated(_wallet, _amount);
        return true;
    }

    function addToAllowList (address _address) public returns(bool) {
        require(msg.sender == owner);
        allowList[_address] = true;
        emit AddToAllowList(_address);
        return true;
    }
    
    function removeInAllowList (address _address) public returns(bool) {
        require(msg.sender == owner);
        allowList[_address] = false;
        emit RemoveInAllowList(_address);
        return true;
    }
}
