pragma solidity ^0.5.8;

contract RockiWallet {
    address public owner;
    uint8 public decimals = 18;

    mapping (address => bool) public allowList;
    mapping (string => bool) public depositHash;

    mapping (address => Wallet) public mainNetWallet;
    mapping (address => Wallet) public privateNetWallet;

    mapping (address => bool) public registeredWallet;

    constructor() public {
        owner = msg.sender;
        allowList[msg.sender] = true;
    }

    struct Wallet {
        address mainNetAddress;
        address privateNetAddress;
        uint256 amount;
    }

    event AddToAllowList(address wallet);
    event RemoveInAllowList(address wallet);
    event BalancePlus(address mainNetAddress, address privateNetAddress, uint256 amount);
    event BalanceMinus(address mainNetAddress, address privateNetAddress, uint256 amount);
    event DepositCreated(address mainNetAddress, address privateNetAddress, string hash, uint256 amount);

    modifier isAllowList() { 
        require (allowList[msg.sender] == true, 'Your address is not in the list of allowed!'); 
        _; 
    }

    modifier isNotProcessed(string memory txHash) { 
        require (depositHash[txHash] == false, 'This request is processed!');
        _; 
    }
    
    modifier isOwner() { 
        require (msg.sender == owner, 'You are not the owner of the contract!'); 
        _; 
    }

    modifier isNotRegistered(address privateNetAddress) { 
        require (registeredWallet[privateNetAddress] == false, 'This account is registered!'); 
        _; 
    }

    modifier isRegistered(address privateNetAddress) {
        require (registeredWallet[privateNetAddress] == true, 'This account not registered!'); 
        _; 
    }

    function newWallet (address _mainNetAddress, address _privateNetAddress) public isAllowList() isNotRegistered(_privateNetAddress) returns (bool res) {
        registeredWallet[_privateNetAddress] = true;
        Wallet memory _wallet = Wallet(_mainNetAddress, _privateNetAddress, 0);
        mainNetWallet[_mainNetAddress] = _wallet;
        privateNetWallet[_privateNetAddress] = _wallet;
        return true;
    }

    function balancePlusByMainNet (address _mainNetAddress, uint256 _amount) public isAllowList() returns(bool) {
        require (_amount > 0, 'Amount less zero!');
        Wallet memory _wallet = mainNetWallet[_mainNetAddress];
        require (registeredWallet[_wallet.privateNetAddress] == true, 'This account is not registered!');
        _wallet.amount += _amount;
        mainNetWallet[_mainNetAddress] = _wallet;
        privateNetWallet[_wallet.privateNetAddress] = _wallet;
        emit BalancePlus(_mainNetAddress, _wallet.privateNetAddress, _amount);
        return true;
    }

    function balanceMinusByMainNet (address _mainNetAddress, uint256 _amount) public isAllowList() returns(bool) {
        require (_amount > 0, 'Amount less zero!');
        Wallet memory _wallet = mainNetWallet[_mainNetAddress];
        require (registeredWallet[_wallet.privateNetAddress] == true, 'This account is not registered!');
        require (_wallet.amount >=_amount, 'Not enough funds on the balance sheet!');
        _wallet.amount -= _amount;
        mainNetWallet[_mainNetAddress] = _wallet;
        privateNetWallet[_wallet.privateNetAddress] = _wallet;
        emit BalanceMinus(_mainNetAddress, _wallet.privateNetAddress, _amount);
        return true;
    }

    function balancePlus (address _mainNetAddress, address _privateNetAddress, uint256 _amount) isAllowList() public returns(bool) {
        require(_amount > 0, 'Amount less zero!');
        Wallet memory _wallet = privateNetWallet[_privateNetAddress];
        _wallet.amount += _amount;
        mainNetWallet[_mainNetAddress] = _wallet;
        privateNetWallet[_privateNetAddress] = _wallet;
        emit BalancePlus(_mainNetAddress, _privateNetAddress, _amount);
        return true;
    }

    function balanceMinus (address _mainNetAddress, address _privateNetAddress, uint256 _amount) isAllowList() public returns(bool) {
        require(_amount > 0, 'Amount less zero!');
        Wallet memory _wallet = privateNetWallet[_privateNetAddress];
        require(_wallet.amount >= _amount, 'Not enough funds on the balance sheet!');
        _wallet.amount -= _amount;
        mainNetWallet[_mainNetAddress] = _wallet;
        privateNetWallet[_privateNetAddress] = _wallet;
        emit BalanceMinus(_mainNetAddress, _privateNetAddress, _amount);
        return true;
    }

    function deposit(
        string memory _hash, 
        address _mainNetAddress, 
        address _privateNetAddress, 
        uint256 _amount
    ) isAllowList() isRegistered(_privateNetAddress) isNotProcessed(_hash) public returns(bool) {
        depositHash[_hash] = true;
        Wallet memory _wallet = privateNetWallet[_privateNetAddress];
        _wallet.amount += _amount;
        mainNetWallet[_mainNetAddress] = _wallet;
        privateNetWallet[_privateNetAddress] = _wallet;
        emit DepositCreated(_mainNetAddress, _privateNetAddress, _hash, _amount);
        return true;
    }
    
    function addToAllowList (address _address) isOwner() public {
        allowList[_address] = true;
        emit AddToAllowList(_address);
    }
    
    function removeInAllowList (address _address) isOwner() public {
        allowList[_address] = false;
        emit RemoveInAllowList(_address);
    }
    
    function balanceOf(address _privateNetAddress) public view returns (uint256) {
        return privateNetWallet[_privateNetAddress].amount;
    }
}
